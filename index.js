#!/usr/bin/env node

var prompt = require('prompt');
const rimraf = require('rimraf');
const ncp = require('ncp');
const fs = require('fs');
const changeCase = require('change-case');
const readline = require('readline');
const { exec } = require('child_process');

const cwd = process.cwd()
var properties = [
   {
     name: 'name',
     type: 'string',
     required: true
   },
   {
     name: 'version',
     default: '1.0.0'
   },
   {
     name: "description",
     default: ""
   },
   {
     name: "main",
     default: "index.js"
   },
   {
     name: "author",
     default: ""
   },
   {
      name: "license",
      "default": "ISC"
   },
   {
     name: "apiList",
     "default": "api adminApi",
     "type": "string",
     "required": true,
     "message": "Enter the number of levels of Api's using space as delimeter"
   },

 ];

 prompt.start();

 prompt.get(properties, processCommands);

async function processCommands(err, commandObject){
   if(err){
     return onErr(err)
   }
   commandObject.apiList = commandObject.apiList.split(" ")
   try{
     await deleteDirectory(commandObject.name)
     await copyTemplate(commandObject.name)
     await createPackageJsonFile(commandObject)
     await createApiDirectories(commandObject)
     await createServerFile(commandObject)
     await createReadMeFile(commandObject)
     prompt.get([{name: "confirmation", description: "Do you want to install dependencies? (Y/N)", type: "string", default: 'Y', required: true}], function(err, result){
       let answer = result.confirmation
       if(answer.toLowerCase() == "y" ||  answer.toLowerCase() == "yes"){
         process.chdir('./'+commandObject.name)
         console.log("installing node dependencies...");
         exec('npm run dependencies', (err, stdout, stderr) => {
           if (err) {
              return;
            }
            // the *entire* stdout and stderr (buffered)
            console.log(`stdout: ${stdout}`);
            console.log(`stderr: ${stderr}`);
        });
       }else{
         console.log("you can install dependencies by running: npm run dependencies");
       }
     })
   }catch(error){
     console.log(error)
   }

}

function createReadMeFile(processCommands){
  return new Promise(function(resolve, reject) {
    let data = fs.readFileSync(cwd+"/"+processCommands.name+"/README.md", 'utf-8')
    let newValue = data.replace(/{{appName}}/g, processCommands.name)
    let apidoc = "", checkDoc = ""
    for(let api of processCommands.apiList){
      apidoc += "npm run apidoc-"+api+"\n"
      checkDoc += "http://localhost:5000/public/"+changeCase.paramCase(api)+"/docs\n"
    }
    newValue = newValue.replace(/{{apidoc}}/g, apidoc)
    newValue = newValue.replace(/{{apidocUrl}}/g, checkDoc)
    fs.writeFileSync(cwd+"/"+processCommands.name+"/README.md", newValue)
    resolve()
  });
}

function createServerFile(processCommands){
  return new Promise(function(resolve, reject) {
    let replaceString = "",apiList=processCommands.apiList
    for(let i=0,length=apiList.length; i<length; i++){
      let api = changeCase.paramCase(apiList[i])
      replaceString += "app.use('/"+api+"/auth', require('../../app/auth/"+apiList[i]+"Auth'))\n"
      replaceString += "app.use('/"+api+"', require('../../app/"+apiList[i]+"'))\n"
    }
    let data = fs.readFileSync(__dirname+'/general/server.js', 'utf-8');

    var newValue = data.replace("{{}}", replaceString);

    fs.writeFileSync(cwd+"/"+processCommands.name+"/config/initializers/server.js", newValue, 'utf-8');
    resolve()
  });
}

function createApiDirectories(processCommands){
  return new Promise(function(resolve, reject) {
    for(let i=0,length=processCommands.apiList.length; i<length; i++){
      let subDir = cwd+"/"+processCommands.name+"/app"
      let dirPath = cwd+"/"+processCommands.name+"/app/"+processCommands.apiList[i]
      fs.mkdirSync(dirPath);
      fs.createReadStream(__dirname+"/general/routes.js").pipe(fs.createWriteStream(dirPath+"/index.js"));
      fs.createReadStream(__dirname+"/general/auth.js").pipe(fs.createWriteStream(subDir+"/auth/"+processCommands.apiList[i]+"Auth.js"));
    }
    resolve()
  });
}

function deleteDirectory(path){
  return new Promise(function(resolve, reject) {
    rimraf(path, function(err){
      if(err){
        reject(err)
        return
      }
      resolve()
    });
  });
}

function copyTemplate(appName){
  return new Promise(function(resolve, reject) {
    ncp(__dirname+"/template", cwd+"/"+appName, function(err){
      if(err){
        reject(err)
        return
      }
      resolve()
    })
  });
}

function createPackageJsonFile(processCommands){
  return new Promise(function(resolve, reject) {
    let packageObject = getPackageJsonObject(processCommands)
    fs.writeFile(cwd+"/"+processCommands.name+"/package.json", JSON.stringify(packageObject, null, 4), function(err) {
      if(err) {
        reject(err)
        return
      }
      resolve()
    });
  });
}

 function getPackageJsonObject(processCommands){
   let packageObject = {
     "name": processCommands.name,
     "version": processCommands.version,
     "description": processCommands.description,
     "main": processCommands.main,
     "author": processCommands.author,
     "license": processCommands.license,
     "apidoc":{
       "title": processCommands.name+" Api Documentation"
     }
   }

   let scriptObject = {
     "start": "pm2 delete "+packageObject.name+"; NODE_ENV=production pm2 start index.js --name '"+packageObject.name+"'",
     "heroku": "NODE_ENV=production HEROKU=true node index.js",
     "dev": "NODE_ENV=development nodemon index.js",
     "test": "node_modules/.bin/mocha",
     "migrations": "NODE_ENV=development node scripts/migrate.js",
     "artillery": "cd artillery; artillery run -k payload.json ",
     "dependencies": "npm install apidoc body-parser change-case cors dotenv eko-joi-objectid email-validator express joi joi-phone jsonwebtoken mocha mongodb mongodb-backup mongodb-restore mongoose morgan nconf password-hash path require-dir rimraf should winston --save"
   }

   let apiDocExcludeString = []
   for(let i=0,length=processCommands.apiList.length; i<length; i++){
     apiDocExcludeString[i] = ""
     for(let j=0,length2=processCommands.apiList.length; j<length2; j++){
       if(i !== j){
         apiDocExcludeString[i] += " -e app/auth/"+processCommands.apiList[j]+"Auth.js -e app/"+processCommands.apiList[j]+" "
       }
     }
   }

   for(let i=0,length=processCommands.apiList.length; i<length; i++){
     scriptObject["apidoc-"+processCommands.apiList[i]] = "node_modules/apidoc/bin/apidoc -i app "+apiDocExcludeString[i]+" -o public/"+changeCase.paramCase(processCommands.apiList[i])+"/docs"
   }

   packageObject.scripts = scriptObject
   return packageObject
 }

 function onErr(err) {
   console.log(err);
   return 1;
 }
