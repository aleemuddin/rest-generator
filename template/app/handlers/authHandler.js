const logger = require('winston');
const basicHelper = require('../helpers/basicHelper');

const authorizeUser = async (request, response) => {
  let access_token = request.headers.access_token
  if(!access_token){
    return basicHelper.generateUnauthorizedResponse(response, new Error("access_token header is required"));
  }

  let decoded
  try{
    decoded = basicHelper.verifyToken(access_token)
  }catch(err){
    return basicHelper.generateUnauthorizedResponse(response, new Error("invalid access_token"));
  }

  //Write your any initilization logic here

  response.locals.decoded = decoded
  next()
}

const authHandler = {

}

module.exports = authHandler
