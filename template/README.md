# {{appName}}
[![N|Solid](https://cldup.com/dTxpPi9lDf.thumb.png)](https://nodesource.com/products/nsolid)

### Installation

spotify requires [Node.js](https://nodejs.org/) v8+ to run.

Install the dependencies and devDependencies and start the server.

```sh
$ cd {{appName}}
$ npm install
$ npm run dev
```

For production environments...

```sh
$ npm install
$ npm start
```

### Generating Docs
```sh
{{apidoc}}
```
### Checking Docs
{{apidocUrl}}
